__precompile__()

include("../utils/most_common_element.jl")


function entropy(y)
    res = 0
    for val in unique(y)
        p = count(y .== val)/length(y)
        if p != 0.0
            res -= p * log(2,p)
        end
    end
    return res
end

function gini(y)
    res = 0
    for val in unique(y)
        p = count(y .== val)/length(y)  # todo mb devectorize
        res += p^2
    end
    return 1 - res
end

function weighted_criterion(part, l, criterion)
    return length(part)/l * criterion(part)
end

"Decision stump"
type DecisionStump
    best_gain::Any
    split_value::Any
    split_index::Int64

    left_class::Any
    right_class::Any

    _criterion::Function

    fit::Function
    predict::Function

    function DecisionStump(;criterion="gini")
        this = new()

        # todo rewrite to dict
        this._criterion = criterion == "entropy" ? entropy : gini 

        this.fit = function (X_train, y_train)
            return fit!(this, X_train, y_train)
        end
        this.predict = function (X_test)
            return predict(this, X_test)
        end
        return this
    end

end

function fit!(clf::DecisionStump, X_train, y_train)
    if ndims(X_train) == 1
       X_train = X_train[:,:] 
    end
    if ndims(y_train) != 1
        error("y_train should be 1-dimensional array for Simple Linear regression")
    end

    # todo make separate function
    X = X_train
    y = y_train

    if length(y) <= 1
        return nothing # todo add behavior
    end

    parent_impurity = clf._criterion(y)
    best_gain = -parent_impurity
    best_split_value = 0  # should be replaced anyway
    best_index = 1
    left_class = y[1]
    right_class = y[1]

    l = length(y) 

    # parent score is equal because this is decision stump
    function gain(left, right) # todo check performance
        return -length(left)/l * clf._criterion(left) - 
                length(right)/l * clf._criterion(right)
    end    

    for i in 1:size(X,2)
        column = X[:,i]
        for value in column
            #println(value)
            left = column .<= value
            right = .!(left)
            y_left = y[left]
            y_right = y[right]

    #         score = length(y_left)/length(y) * gini(y_left) + length(y_right)/length(y) * gini(y_right)
            #score = weighted_gini(y_left, l) + weighted_gini(y_right, l) 
            score = gain(y_left, y_right)
            #println("Score is $score")
            if score > best_gain
                best_gain = score
                #best_split = left, right, value # tido compare performance
                best_split_value = value
                best_index = i
                left_class = get_most_common_element(y_left)
                right_class = get_most_common_element(y_right)
            end
        end
    end
    clf.best_gain = parent_impurity + best_gain
    clf.split_index = best_index
    clf.split_value = best_split_value
    clf.left_class = left_class
    clf.right_class = right_class
    #return best_gain, best_value, best_index, left_class, right_class  # todo mb rename to split_value
end

function predict(clf::DecisionStump, X_test)
    if ndims(X_test) == 1
       X_train = X_train[:,:] 
    end
    return [el <= clf.split_value ? clf.left_class : clf.right_class for el in X_test[:, clf.split_index]]
end

