__precompile__()

function print_node(node, indent=""; left=true)
    if node.is_leaf
        leaf_str = string(indent, " leaf: $(node.value)")
        println(leaf_str)
    else
		println(indent, " X[$(node.split_index)] <= $(node.threshold)")
        
        indent = string(indent, " \|")
        println(indent, "\\left: ")
        print_node(node.left_branch, indent)
        println(replace(string(indent, "\\right: "), "\|\\", " \\"))
        indent = string(indent[1:end-1], " ")
        print_node(node.right_branch, indent, left=false)
    end
end

function predict_and_print_node(node, row, predicted_branch=true, indent=""; left=true)
    if node.is_leaf
        leaf_str = predicted_branch ? " *LEAF: $(node.value)*" : " leaf: $(node.value)"
        leaf_str = string(indent, leaf_str)
        println(leaf_str)
    else
        println(indent, " X[$(node.split_index)] <= $(node.threshold)")
        go_left = predicted_branch ? (row[node.split_index] <= node.threshold) : false
        go_right = predicted_branch ? !go_left : false

        indent = string(indent, " \|")
        println(indent, "\\left: ")
        predict_and_print_node(node.left_branch, row, go_left, indent)
        println(replace(string(indent, "\\right: "), "\|\\", " \\"))
        indent = string(indent[1:end-1], " ")
        predict_and_print_node(node.right_branch, row, go_right, indent, left=false)
    end
end

function predict_and_print_node_unicode(node, row, predicted_branch=true, indent=""; left=true)
    if node.is_leaf
        leaf_str = predicted_branch ? " *LEAF: $(node.value) ✅" : " leaf: $(node.value)"
        println(indent, leaf_str)
    else
        println(indent, " X[$(node.split_index)] <= $(node.threshold)")
        go_left = predicted_branch ? (row[node.split_index] <= node.threshold) : false
        go_right = predicted_branch ? !go_left : false

        indent = string(indent, (predicted_branch && go_right) ? " \⏐" : " \|")

        left_text = string(indent, (predicted_branch && go_left) ? "\ ⃥left: " : "\\left: ")
        println(left_text)
        predict_and_print_node_unicode(node.left_branch, row, go_left, indent)

        sym = go_right ? "\⏐\ ⃥" : "\|\\"
        right_prefix = go_right ? "\ ⃥right: " : "\\right: "
        right_text = replace(string(indent, right_prefix), sym, go_right ? " \ ⃥" :" \\")
        println(right_text)
        indent = string(indent[1:end-1], " ")
        predict_and_print_node_unicode(node.right_branch, row, go_right, indent, left=false)
    end
end
