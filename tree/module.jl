__precompile__()

"Tree models module"
module tree
export DecisionStump, DecisionTreeClassifier 
include("decision_stump.jl")
include("decision_tree.jl")
end
