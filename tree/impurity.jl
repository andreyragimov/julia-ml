__precompile__()

function entropy(y)
    res = 0
    for val in unique(y)
        p = count(y .== val)/length(y)
        if p != 0.0
            res -= p * log(2,p)
        end
    end
    return res
end

function gini(y)
    res = 0
    for val in unique(y)
        p = count(y .== val)/length(y)  # todo mb devectorize
        res += p^2
    end
    return 1 - res
end



function criteria_with_weights(y, weights, criteria)
    res = 0
    for val in unique(y)
        indexes = y .== val
        if isempty(weights)
            p = count(indexes)/length(y)  # todo mb devectorize
        else
            p = sum(weights[indexes])
        end
        
        if criteria == "gini"
            res += p^2
        elseif p != 0.0  # entropy
            res -= p * log(2,p)
        end
    end
    return criteria == "gini" ? 1 - res : res
end


function information_gain(left, y, weights, criteria="gini")
    l = length(y)
    right = .!(left)
    y_left = y[left]
    y_right = y[right]
    
    # todo refactor
    w_l = isempty(weights) ? weights : weights[left] ./ sum(weights[left])
    w_r = isempty(weights) ? weights : weights[right] ./ sum(weights[right])
    #score = gini_with_weights(y, weights) - (length(y_left)/l * gini_with_weights(y_left, w_l) + length(y_right)/l * gini_with_weights(y_right, w_r))
    parent_score = criteria_with_weights(y, weights, criteria)
	score_left = length(y_left)/l * criteria_with_weights(y_left, w_l, criteria)
	score_right = length(y_right)/l * criteria_with_weights(y_right, w_r, criteria)
	return parent_score - (score_left + score_right)
end



function gini_with_weights(y, weights)
    if isempty(weights)
        return gini(y)
    end
    res = 0
    for val in unique(y)
        indexes = y .== val
        p = count(indexes)/length(y)  # todo mb devectorize
        #println("weights are $weights")
        println("p was $p")
        #p = p * sum(weights[indexes])
        p = sum(weights[indexes])
        println("p with weights $p")
        res += p^2
    end
    return 1 - res
end


function weighted_criterion(part, l, criterion)
    return length(part)/l * criterion(part)
end

# todo test performance
function gini_gain(left, right, parent)
    return gini(parent) - length(left)/length(parent) * gini(left) - length(right)/length(parent) * gini(right)
end
