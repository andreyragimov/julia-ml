__precompile__()

using mlkit.utils: get_most_common_element

include("../tree/impurity.jl")
include("../tree/printing.jl")


function weighted_criterion(part, l, criterion)
    return length(part)/l * criterion(part)
end

"Decision Tree Node"
type TreeNode
    split_index::Int64
    threshold::Float64

    is_leaf::Bool
    value::Float64

    left_branch::TreeNode
    right_branch::TreeNode

    function TreeNode(split_index, threshold)
        this = new()
        this.split_index = split_index
        this.threshold = threshold
        this.is_leaf = false

        return this
    end

    function TreeNode(value)
        this = new()
        this.value = value
        this.is_leaf = true

        return this
    end
end

"Decision Tree Classifier"
type DecisionTreeClassifier
    root::TreeNode

    max_depth::Int64
    min_samples_split::Int64
    min_impurity_split::Float64

    splitter::String
    random_state::Int64

    sample_weight::Vector{Float64}

    _current_depth::Int64

    _criterion::String

    _shuffle_indexes::Function

    print_tree::Function
    print_prediction_path::Function

    fit::Function
    predict::Function

    function DecisionTreeClassifier(;criterion="gini", max_depth=-1, min_samples_split=2, min_impurity_split=0, splitter="best", random_state=-1)
        this = new()

        this.max_depth = max_depth
        this.min_samples_split = min_samples_split 
        this.min_impurity_split = min_impurity_split

        if !(splitter in ["random", "best"])
            error("Available options for splitter  are \"best\" and \"random\"")
        end
        this.splitter = splitter  # todo support other splitters 

        this.random_state = random_state
        if random_state != -1
            srand(random_state)
        end

        this._current_depth = 0

        # todo rewrite to dict with errors for invalid values
        if !(criterion in ["entropy", "gini"])
            error("Available options for criterion are \"gini\" and \"entropy\"")
        end
        this._criterion = criterion

        this.sample_weight = []

        this._shuffle_indexes = function (l)
            return shuffle(1:l)
        end

        this.print_tree = function ()
            return print_node(this.root)
        end
        this.print_prediction_path = function (row)
            return predict_and_print_node_unicode(this.root, row)
        end

        this.fit = function (X_train, y_train; sample_weight=[])
            return fit!(this, X_train, y_train; sample_weight=sample_weight)
        end
        this.predict = function (X_test)
            return predict(this, X_test)
        end
        return this
    end

end

function information_gain_old(left, y, weights)
    l = length(y)
    right = .!(left)
    y_left = y[left]
    y_right = y[right]
    
    parent_impurity = gini(y)
    
    w_l = isempty(weights) ? weights : weights[left] ./ sum(weights[left])
    w_r = isempty(weights) ? weights : weights[right] ./ sum(weights[right])
    
    score = gini_with_weights(y, weights) - (length(y_left)/l * gini_with_weights(y_left, w_l) + length(y_right)/l * gini_with_weights(y_right, w_r))
    return score
end
        

function create_leaf(y)
    return TreeNode(get_most_common_element(y))
end

function weighted_gini(part, l)
    return length(part)/l * gini(part)
end

function split_tree_data(clf::DecisionTreeClassifier, X, y, sample_weight)

    if length(y) == 0
        error("y is empty")
    end

    parent_impurity = gini(y)
    if parent_impurity <= clf.min_impurity_split
        return create_leaf(y)
    end

    if length(y) <= clf.min_samples_split
        print(".")  # indicating training process
        return create_leaf(y)
    end

    if clf.max_depth > 0 && clf._current_depth >= clf.max_depth 
        print("_")  # indicating training process
        return create_leaf(y)
    end

    best_gain = -parent_impurity
    best_split_value = 0  # should be replaced anyway
    best_index = 1
    previous_split_is_best = false

    l = length(y) 
    
	left_part = nothing
    indexes = clf._shuffle_indexes(size(X,2))
    if clf.splitter == "random"
        indexes = indexes[:1]
    end
    for i in indexes  # todo mb replace to function returning all indexes or specified/random index
        column = X[:,i]
        for value in sort(unique(column))
            left = column .<= value

            score = information_gain(left, y, sample_weight, clf._criterion)

            right = .!(left)
            y_left = y[left]
            y_right = y[right]

            # it's better to split between values (for 5 and 6 prefer split value 5.5) 
            if previous_split_is_best
               best_split_value = (best_split_value + value) / 2 
            end
            previous_split_is_best = false

            if score > best_gain
                if sum(left) < 1 || sum(.!(left)) < 1 # todo refactor to constant
                    continue
                end
                best_gain = score
                best_split_value = value
                best_index = i
				left_part = left
            
                previous_split_is_best = true
            end
        end
    end

    if best_gain <= clf.min_impurity_split
        return create_leaf(y)
    end

	left = left_part
	right = .!(left)

    clf._current_depth += 1


    # todo refactor
    weights = sample_weight
    w_l = isempty(weights) ? weights : weights[left]
    w_r = isempty(weights) ? weights : weights[right]

    node = TreeNode(best_index, best_split_value)
    node.left_branch = split_tree_data(clf, X[left,:], y[left], w_l)
    node.right_branch = split_tree_data(clf, X[right,:], y[right], w_r)
    
    return node
end


function node_predict(node::TreeNode, row::Array{Float64})
    if node.is_leaf
        return node.value
    else
        @inbounds @fastmath if row[node.split_index] <= node.threshold
            return node_predict(node.left_branch, row)
        else
            return node_predict(node.right_branch, row)
        end
    end
end

function fit!(clf::DecisionTreeClassifier, X_train, y_train; sample_weight=[])
    clf.sample_weight = sample_weight  # not accesed in the code currently, just passing sample_weights between methods. This could be useful to see tree weights
    clf._current_depth = 0 
    clf.root = split_tree_data(clf, X_train, y_train, sample_weight)
end

function predict(clf::DecisionTreeClassifier, X_test)
    res = Vector{Float64}(size(X_test,1))
    @simd for i in 1:size(X_test,1)
        @inbounds res[i] = node_predict(clf.root, X_test[i,:])
    end
    return res
end
