__precompile__()

using Base.Test
# Modules with python-style interfaces, are simple to import
# Usage: clf.fit(X_train, y_train) and so on
using mlkit.simple:RandomClassifier, ZeroRuleClassifier

# Modules with julia-style interfaces, accessed via include
# Usage: fit!(clf, X_train, y_train) and so on
include("../simple/random_classifier.jl")
include("../simple/zero_rule.jl")

@testset "Simple classifiers" begin
    X_train = [1,2,3,10,20,30,40,50]
    y_train = [0,1,1,1,2,3,4,5]
    X_test = [1,3]
    y_test = [0,1]
    
    @testset "RandomClassifier" begin
        clf = RandomClassifier(100500)
        clf.fit(X_train, y_train)

        @test clf.markers == Set(y_train)
        @test clf.seed == 100500
        res = clf.predict(X_test)
        # "Let's just test that elements are in valid range"
        for el in res
            @test el in y_train
        end
    end
    @testset "ZeroRuleClassifier" begin
        clf = ZeroRuleClassifier()
        clf.fit(X_train, y_train)

        @test clf.most_common_class == 1
        res = clf.predict(X_test)
        # "Let's just test that elements are in valid range"
        for el in res
            @test el in y_train
        end
    end
end;
