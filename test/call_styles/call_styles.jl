__precompile__()


"""
Modules with python-style interfaces, are simple to import
Usage: 
clf.fit(X_train, y_train) # and so on

Importing:
using mlkit.simple:RandomClassifier, ZeroRuleClassifier

Modules with julia-style interfaces, accessed via include
Usage: 
fit!(clf, X_train, y_train) # and so on

Importing:
include("../simple/random_classifier.jl")
include("../simple/zero_rule.jl")
"""

module CallStylesTest
using Base.Test
@testset "Call styles" begin
    include("python_style_call.jl")
    include("julia_style_call.jl")
end
end
