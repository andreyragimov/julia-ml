__precompile__()

using Base.Test

# Modules with julia-style interfaces, accessed via include
# Usage: fit!(clf, X_train, y_train) and so on
include("../../simple/random_classifier.jl")
include("../../simple/zero_rule.jl")

@testset "Julia-style interface" begin
    X_train = [1,2,3,10,20,30,40,50]
    y_train = [0,1,1,1,2,3,4,5]
    X_test = [1,3]
    y_test = [0,1]

    classifier_types = [RandomClassifier, ZeroRuleClassifier]
    for clf_type in classifier_types
        clf = clf_type()
        fit!(clf, X_train, y_train)
        y_pred = predict(clf, X_test)
        for el in y_pred
            @test el in y_train
        end
    end
end
