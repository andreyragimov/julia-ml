__precompile__()

using Base.Test
using mlkit.linear_model: LinearRegression, BasicLinearRegression
using mlkit.simple:MeanRegression

# simple rmse just for simple tests todo move to utils
function simple_rmse(y_true, y_pred)
    return sqrt( sum(( (y_true[i]-y_pred[i])^2 for i in 1:size(y_true,1) )) )
end

@testset "LinearModels" begin
    @testset "TinyDataset" begin
        # just simple y = 2*x+1 like tiny dataset for first test, better use generated or csv datasets
        X_train = [1,2,3,4,5]
        y_train = [3,4,8,9,11]
        X_test = [6,7,8,9,10]
        y_test = [14,15,18,19,21]

        @testset "BasicLinearModel" begin
            regr = BasicLinearRegression()
            regr.fit(X_train, y_train)
            # both should be in some expected range
            @test 0 < regr.b0 < 1.5 
            @test 1 < regr.b1 < 3
            y_pred = regr.predict(X_test)

            # Comparsion with naive model y = 2x+1
            rmse_result = simple_rmse(y_test, y_pred)
            y_pred_naive = [13,15,17,19,21]
            rmse_naive = simple_rmse(y_test, y_pred_naive)
            @test rmse_result < rmse_naive

            # Comparsion with model that just takes mean of y_train
            regr = MeanRegression()
            regr.fit(X_train, y_train)
            y_pred_mean = regr.predict(X_test)
            rmse_mean = simple_rmse(y_test, y_pred_mean)
            @test rmse_result < rmse_mean

        end
        @testset "LinearRegression" begin
            regr = LinearRegression()
            regr.fit(X_train, y_train)
            # both should be in some expected range
            @test 0 < regr.intercept_[1] < 1.5 
            @test 1 < regr.coef[1] < 3
            y_pred = regr.predict(X_test)

            # Comparsion with naive model y = 2x+1
            rmse_result = simple_rmse(y_test, y_pred)
            y_pred_naive = [13,15,17,19,21]
            rmse_naive = simple_rmse(y_test, y_pred_naive)
            @test rmse_result < rmse_naive

            # Comparsion with model that just takes mean of y_train
            regr = MeanRegression()
            regr.fit(X_train, y_train)
            y_pred_mean = regr.predict(X_test)
            rmse_mean = simple_rmse(y_test, y_pred_mean)
            @test rmse_result < rmse_mean

            # Comparsion with BasicLinearRegression
            regr = BasicLinearRegression()
            regr.fit(X_train, y_train)
            y_pred_basic = regr.predict(X_test)
            @test y_pred ≈ y_pred_basic
        end
    end
    @testset "Linerrud dataset from sklearn" begin
        # copy of train/test split of linerrud dataset
        X_train = [
					8.0  101.0   38.0
					1.0   50.0   50.0
					13.0  155.0   58.0
					12.0  101.0  101.0
					4.0  101.0   42.0
					6.0   70.0   31.0
					17.0  251.0  250.0
					6.0  125.0   40.0
					4.0   60.0   25.0
					13.0  210.0  115.0
					12.0  105.0   37.0
					5.0  162.0   60.0
					12.0  210.0  120.0
					14.0  215.0  105.0]
        y_train = [
					211.0  38.0  56.0
					247.0  46.0  50.0
					189.0  35.0  46.0
					193.0  38.0  58.0
					182.0  36.0  56.0
					193.0  36.0  46.0
					154.0  33.0  56.0
					167.0  34.0  60.0
					176.0  37.0  54.0
					166.0  33.0  52.0
					162.0  35.0  62.0
					191.0  36.0  50.0
					202.0  37.0  62.0
					154.0  34.0  64.0]
        X_test = [
					15.0  225.0  73.0
					 2.0  110.0  60.0
					 2.0  110.0  43.0
					15.0  200.0  40.0
					17.0  120.0  38.0
					11.0  230.0  80.0]
        y_test = [
					156.0  33.0  54.0
					189.0  37.0  52.0
					138.0  33.0  68.0
					176.0  31.0  74.0
					169.0  34.0  50.0
					157.0  32.0  52.0
]
        @testset "LinearRegression" begin
            regr = LinearRegression()
            regr.fit(X_train, y_train)
            y_pred = regr.predict(X_test)
            
            # on linnerud dataset R2 score is lower than zero for default sklearn LR so basic mean is actually performs better
            # rmse_result = simple_rmse(y_test, y_pred)
            # y_pred_naive = repeat(mean(y_test,1); outer=[size(X_test,1), 1]) 
            # rmse_naive = simple_rmse(y_test, y_pred_naive)
            # @test rmse_result < rmse_naive
        end
    end
end
