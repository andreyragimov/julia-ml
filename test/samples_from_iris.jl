__precompile__()

using Base.Test
using mlkit.neighbors: KNeighborsClassifier 
using mlkit.tree: DecisionStump 

"simple accuracy score function for measuring accuracy in tests"
function accuracy(y_test, y_pred)
    return sum(y_pred .== y_test) / length(y_test)
end

@testset "Samples from Iris dataset" begin
    X_train = [
                 6.7  3.3  5.7  2.5
				 7.7  3.8  6.7  2.2
				 6.4  3.2  5.3  2.3
				 4.6  3.6  1.0  0.2
				 5.2  3.4  1.4  0.2
				 4.4  3.0  1.3  0.2
				 4.9  2.4  3.3  1.0
				 6.4  2.7  5.3  1.9
				 6.1  3.0  4.6  1.4
				 6.8  3.0  5.5  2.1
               ]
    y_train = [
				 2
				 2
				 2
				 0
				 0
				 0
				 1
				 2
				 1
				 2
]
    X_test = [
				 6.3  3.4  5.6  2.4
				 5.8  2.7  5.1  1.9
				 5.1  3.4  1.5  0.2
				 5.1  3.8  1.9  0.4
				 7.0  3.2  4.7  1.4]
    y_test = [ 
				 2
				 2
				 0
				 0
				 1]
    @testset "KNN Classifier" begin
        clf = KNeighborsClassifier(3)
        clf.fit(X_train, y_train)

        y_pred = clf.predict(X_test)
        # "Let's just test that elements are in valid range"
        for el in y_pred
            @test el in y_train
        end
        acc = accuracy(y_test, y_pred)
        println("predictions: $y_pred, accuracy is $acc")
        @test acc > 1/3  # zero rule accuracy
        @test acc >= 0.8 # 1 mislabeled class
    end        
    @testset "Decision Stump gini" begin
        clf = DecisionStump()
        clf.fit(X_train, y_train)

        y_pred = clf.predict(X_test)
        # "Let's just test that elements are in valid range"
        for el in y_pred
            @test el in y_train
        end
        acc = accuracy(y_test, y_pred)
        println("predictions: $y_pred, accuracy is $acc")
        @test acc > 1/3  # zero rule accuracy
        @test acc >= 0.4 # 3 mislabeled classes
    end        
    @testset "Decision Stump entropy" begin
        clf = DecisionStump(criterion="entropy")
        clf.fit(X_train, y_train)

        y_pred = clf.predict(X_test)
        # "Let's just test that elements are in valid range"
        for el in y_pred
            @test el in y_train
        end
        acc = accuracy(y_test, y_pred)
        println("predictions: $y_pred, accuracy is $acc")
        @test acc > 1/3  # zero rule accuracy
        @test acc >= 0.4 # 3 mislabeled classes
    end        
end;
