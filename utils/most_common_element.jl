
# Seems like that this is fastest of the implementations presented in list
function get_most_common_element(list)
    y = sort(list)
    max_count = 1
    count = 1
    l = length(y)
    val = y[1]
    most_common_val = val
    for i = 2:l
        if y[i] == val
            count += 1
            # Looks like this early exit slows down a bit so it is removed
#             if max_count >= l - i
#                 return most_common_val
#             end
        else
            if count > max_count
                max_count = count
                most_common_val = val
            end
            val = y[i]
            count = 1
        end
    end
    if count > max_count
        return val
    end
    return most_common_val
end

"Less code but also less efficient as it seems"
function get_most_common_via_sum(y)
    max_count = 1
    most_common_val = y[1]
    
    for val in unique(y)
        count = sum(y .== val)
        if count > max_count
            max_count = count
            most_common_val = val
        end
    end
    return most_common_val
end
