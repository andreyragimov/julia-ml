__precompile__()

"Simple model that just predicts mean for regression task" 
type MeanRegression
    mean::Any
    fit::Function
    predict::Function

    function MeanRegression()
        this = new()
        this.fit = function (X_train, y_train)
            return fit!(this, X_train, y_train)
        end
        this.predict = function (X_test)
            return predict(this, X_test)
        end
    return this
end

end

function fit!(regr::MeanRegression, X_train, y_train)
    regr.mean = mean(y_train,1)
    return
end

function predict(regr::MeanRegression, X_test)
    return repeat(regr.mean; outer=[size(X_test,1), 1]) 
end
