__precompile__()

"Simple random classifier"
type RandomClassifier
    # assume -1 for using without random seed, Easier then using nullable
    seed::Int64
    markers::Set
    fit::Function
    predict::Function

    function RandomClassifier(seed::Int64, markers::Set)
        this = new()
        this.seed = seed
        this.markers = markers
        this.fit = function (X_train, y_train)
            return fit!(this, X_train, y_train)
        end
        this.predict = function (X_test)
            return predict(this, X_test)
        end
        return this
    end

end

function fit!(clf::RandomClassifier, X_train, y_train)
    if clf.seed != -1
        srand(clf.seed)
    end
    clf.markers = Set(y_train)
    return
end

function predict(clf::RandomClassifier, X_test)
    return [rand(clf.markers) for i in 1:size(X_test,1)]
end

# todo check is it better to use args like this or default ones, especially for Set()
RandomClassifier(seed::Int64) = RandomClassifier(seed, Set())
RandomClassifier() = RandomClassifier(-1, Set())

