__precompile__()

"Zero Rule Classifier" 
type ZeroRuleClassifier
    most_common_class::Any
    fit::Function
    predict::Function

    function ZeroRuleClassifier()
        this = new()
        this.fit = function (X_train, y_train)
            return fit!(this, X_train, y_train)
        end
        this.predict = function (X_test)
            return predict(this, X_test)
        end
        return this
    end

end

function fit!(clf::ZeroRuleClassifier, X_train, y_train)
    pairs = [(sum(y_train .== i),i) for i in unique(y_train)]
    clf.most_common_class = maximum(pairs)[2]  # tuples are comparable
    return
end

function predict(clf::ZeroRuleClassifier, X_test)
    return [clf.most_common_class for x in 1:size(X_test,1)]
end
