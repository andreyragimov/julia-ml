__precompile__()

"Simple classifiers module"
module simple
export RandomClassifier
export ZeroRuleClassifier
export MeanRegression
include("random_classifier.jl")
include("zero_rule.jl")
include("mean_regression.jl")
end
