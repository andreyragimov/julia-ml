__precompile__()

# todo mb use module instead of include, compare them(performance/usability/etc)
include("../metrics/distances.jl")

# todo move this function to utils
function _get_most_common_label(pairs)
    d = Dict()
    for (key,value) in pairs
        d[key] = get(d,key,0) + 1
    end
    sort(collect(d), by=x->x[2])[end][1]
end


"Basic K NearestNeighbors Classifier (works slower)" 
type BasicKNeighborsClassifier
    n_neighbors::Int64
    metric::String
    _get_distance::Function

    X_train::Any
    y_train::Any

    fit::Function
    predict::Function

    function BasicKNeighborsClassifier(;n_neighbors::Int64=1, metric::String="l2")
        this = new()

        this.n_neighbors = n_neighbors
        this.metric = metric
        this._get_distance = get_metric(metric)

        this.fit = function (X_train, y_train)
            return fit!(this, X_train, y_train)
        end
        this.predict = function (X_test)
            return predict(this, X_test)
        end
        return this
end

end

BasicKNeighborsClassifier(n_neighbors::Int64, metric::String) = 
BasicKNeighborsClassifier(;n_neighbors=n_neighbors, metric=metric)
BasicKNeighborsClassifier(n_neighbors::Int64) = BasicKNeighborsClassifier(;n_neighbors=n_neighbors) 

function fit!(clf::BasicKNeighborsClassifier, X_train, y_train)
    if size(X_train,1) != size(y_train,1)
        error("Size of X_train and y_train are not equal")
    end
    if clf.n_neighbors > size(X_train, 1)
        error("K is greater than number of samples in training set")
    end
    
    clf.X_train = X_train
    clf.y_train = y_train
    return
end

# this code is way far from optimal and has to much nested operators
# it is used just for comparsion with main version
function predict(clf::BasicKNeighborsClassifier, X_test)
    predictions = []
    for i in 1:size(X_test, 1)
        vec = X_test[i, :]
        nearest_neighbors = []
        for j in 1:size(clf.X_train,1)
            dist = clf._get_distance(vec, clf.X_train[j,:])
            label = clf.y_train[j]
            
            if length(nearest_neighbors) < clf.n_neighbors || dist < nearest_neighbors[end][2]
                push!(nearest_neighbors, (label, dist))
                sort!(nearest_neighbors, by=x->x[2])
                if length(nearest_neighbors) > clf.n_neighbors
                    nearest_neighbors = nearest_neighbors[1:clf.n_neighbors]
                end
            end
        end
        push!(predictions, _get_most_common_label(nearest_neighbors))
    end
    return predictions  
end
