__precompile__()

"Neighbors-based models"
module neighbors
export BasicKNeighborsClassifier, KNeighborsClassifier
include("basic_knn_classifier.jl")
include("knn_classifier.jl")
end
