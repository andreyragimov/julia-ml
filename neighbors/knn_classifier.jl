__precompile__()

using mlkit.metrics
using mlkit.utils: get_most_common_element


"K NearestNeighbors Classifier" 
type KNeighborsClassifier
    n_neighbors::Int64
    metric::String
    _metric_type::Any

    X_train::Any
    y_train::Any

    fit::Function
    predict::Function

    function KNeighborsClassifier(;n_neighbors::Int64=1, metric::String="l2")
        this = new()

        this.n_neighbors = n_neighbors
        this.metric = metric
        this._metric_type = get_metric_type(metric)

        this.fit = function (X_train, y_train)
            return fit!(this, X_train, y_train)
        end
        this.predict = function (X_test)
            return predict(this, X_test)
        end
        return this
end

end

KNeighborsClassifier(n_neighbors::Int64, metric::String) = 
KNeighborsClassifier(;n_neighbors=n_neighbors, metric=metric)
KNeighborsClassifier(n_neighbors::Int64) = KNeighborsClassifier(;n_neighbors=n_neighbors) 

function fit!(clf::KNeighborsClassifier, X_train, y_train)
    if size(X_train,1) != size(y_train,1)
        error("Size of X_train and y_train are not equal")
    end
    if clf.n_neighbors > size(X_train, 1)
        error("K is greater than number of samples in training set")
    end
    
    clf.X_train = X_train
    clf.y_train = y_train
    return
end

function predict(clf::KNeighborsClassifier, X_test)
    predictions = []
    vecs = clf.X_train'
    metric = clf._metric_type()

    distances = pairwise(metric, X_test', clf.X_train')
    for i in 1:size(distances, 1)
        indexes = sortperm(distances[i,:])
        nearest = clf.y_train[indexes][1:clf.n_neighbors]
        push!(predictions, get_most_common_element(nearest))
    end
    return predictions  
end
