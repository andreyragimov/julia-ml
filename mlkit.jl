__precompile__()

module mlkit
include("utils/module.jl")
include("metrics/module.jl")

include("simple/module.jl")
include("linear_model/module.jl")
include("neighbors/module.jl")
include("tree/module.jl")
include("ensemble/module.jl")
end
