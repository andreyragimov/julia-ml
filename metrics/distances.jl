__precompile__()

function euclidean_distance_simple_implementation(a, b)
    return sqrt(sum(( (a[i] - b[i])^2 for i = 1:size(a,1) )))
end

function euclidean_distance_using_dot_product(a, b)
    diff = a - b
    return sqrt(diff'diff)
end

"Optimized version using built-in norm function"
function euclidean_distance(a, b)
    return norm(a-b)
end

function get_metric(name)
    metric_function = get(metrics_dict, name, false)
    if isa(metric_function, Bool)
        error("Metric $name not found!")
    end
    return metric_function
end

metrics_dict = Dict(
               # Multiple implementations used for performance comparsion
               # Ordered by speed beggining with most optimized one 
               "l2" => euclidean_distance,
               "l2dot" => euclidean_distance_using_dot_product,
               "l2simple" => euclidean_distance_simple_implementation
               ) 
# todo add chebysev, minkowski, cosine and other distances
