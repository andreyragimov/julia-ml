__precompile__()

using Distances

function get_metric_type(name)
    metric_function = get(metric_types_dict, name, false)
    if isa(metric_function, Bool)
        error("Metric $name not found!")
    end
    return metric_function
end

metric_types_dict = Dict(
               "l2" => Euclidean,
               "euclidean" => Euclidean,
               #"manhattan" => , 
               "chebyshev" => Chebyshev, 
               "minkowski" => Minkowski, 
               "wminkowski" => WeightedMinkowski, 
               "seuclidean" => SqEuclidean, 
               #"mahalanobis" =>,
               "cosine" => CosineDist
                # todo add more metrics if needed
               ) 


