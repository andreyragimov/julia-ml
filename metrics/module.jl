__precompile__()

"Different metrics for various applications"
module metrics
export pairwise, get_metric, euclidean_distance, get_metric_type
include("distances.jl")
include("optimized_distances.jl")
end
