__precompile__()

using mlkit.tree: DecisionTreeClassifier


"Estimator wrapper for Random Forest"
type Estimator
    subset_features::Any
    subset_indexes::Any
    
    _estimator::Any

    _seed::Int64

    fit::Function
    predict::Function

    function Estimator(random_state, estimator; estimator_params...)
        this = new()
        this._estimator = estimator(;estimator_params...)
        this._seed = random_state

        this.fit = function (X_train, y_train)
            return fit!(this, X_train, y_train)
        end
        this.predict = function (X_test)
            return predict(this, X_test)
        end

        return this
    end
end


function fit!(clf::Estimator, X_train, y_train)
    if clf._seed == -1
        srand()
    else
        srand(clf._seed)
    end
    n, m = size(X_train)
    max_features = ceil(Int64,sqrt(m)) # todo add max_features parameter and sqrt as default
    num_samples = ceil(Int64, n*2/3)
    clf.subset_features = shuffle(1:m)[1:max_features]    
    clf.subset_indexes = shuffle(1:n)[1:num_samples]    

    clf._estimator.fit(X_train[clf.subset_indexes, clf.subset_features], y_train[clf.subset_indexes])
    return
end

function predict(clf::Estimator, X_test)
    return clf._estimator.predict(X_test[:, clf.subset_features])
end


"Random Forest Classifier" 
type RandomForestClassifier
    n_estimators::Int64

	estimator::Any
	estimator_params::Any

    estimators::Any  # todo make concrete types

    random_state::Int64

    fit::Function
    predict::Function

    function RandomForestClassifier(;n_estimators=10, random_state=-1, estimator="default", params...)
        this = new()

		if estimator == "default"
			estimator = DecisionTreeClassifier
			get_kwargs = function(;kwargs...) return kwargs end
			params = get_kwargs(;splitter="best")
		end

		this.n_estimators = n_estimators
		this.estimator = estimator
		this.estimator_params= params

        this.random_state = random_state

        this.fit = function (X_train, y_train)
            return fit!(this, X_train, y_train)
        end
        this.predict = function (X_test)
            return predict(this, X_test)
        end
        return this
    end

end



function fit!(clf::RandomForestClassifier, X_train, y_train)
    if ndims(X_train) == 1
       X_train = X_train[:,:] 
    end
    estimators = []
    
    seed = clf.random_state
    for i in 1:clf.n_estimators
        seed = clf.random_state == -1 ? seed : seed + 1
        estimator = Estimator(seed, clf.estimator; clf.estimator_params...)
        estimator.fit(X_train, y_train)
        push!(estimators, estimator)
    end
    clf.estimators = estimators
    return
end

function predict(clf::RandomForestClassifier, X_test)
	return vote(X_test, clf.estimators)
end
