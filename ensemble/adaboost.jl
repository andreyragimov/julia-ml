__precompile__()

using mlkit.tree: DecisionTreeClassifier


"AdaBoost implementation for classification via weighted voting" 
type AdaBoostClassifier
    n_estimators::Int64

	estimator::Any
	estimator_params::Any

    estimators::Any  # todo make concrete types
    voting_coefficients::Any

    fit::Function
    predict::Function

    function AdaBoostClassifier(;n_estimators=10, estimator="default", estimator_params...)
        this = new()

		if estimator == "default"
			estimator = DecisionTreeClassifier
			get_kwargs = function(;kwargs...) return kwargs end
			params = get_kwargs(;splitter="random")
		end

		this.n_estimators = n_estimators
		this.estimator = estimator
		this.estimator_params = estimator_params

        this.fit = function (X_train, y_train)
            return fit!(this, X_train, y_train)
        end
        this.predict = function (X_test)
            return predict(this, X_test)
        end
        return this
    end

end



function fit!(clf::AdaBoostClassifier, X_train, y_train)
    X, y = X_train, y_train  # same interface with shorter notation
    estimators = []
    voting_coefficients = []
    N = size(X,1)
    d = ones(N) ./ N
    for i in 1:clf.n_estimators
        est = clf.estimator(;clf.estimator_params...)
        est.fit(X, y, sample_weight=d)
        pred = est.predict(X)
        misclassified = (pred .!= y)
        err = d'misclassified  # weighted
        alpha = (0.5*log((1.0-err) / (err+1e-15)))  # close to zero value to prevent from zero divizion
        d = d .* exp.(-alpha .* (y .* pred))
        d = d ./ sum(d)

        if alpha < 0
            alpha = 0  # reversing predictions is effective only for binary classification
        end

        push!(estimators, est)
        push!(voting_coefficients, alpha)
    end
	clf.estimators = estimators
	clf.voting_coefficients = voting_coefficients
    return
end

function predict(clf::AdaBoostClassifier, X_test)
	return weighted_vote(X_test, clf.estimators, clf.voting_coefficients)
end
