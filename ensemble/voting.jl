__precompile__()

using mlkit.utils: get_most_common_element


function weighted_vote(X, estimators, voting_coefficients=[])
    if isempty(voting_coefficients)
        voting_coefficients = [1 for i in 1:length(estimators)]
    end
    votes = hcat((h.predict(X) for h in estimators)...)
    y_pred = Vector(size(X,1))
    for i in 1:size(votes,1)
        column = votes[i,:]
        best = -1 
        res = column[1]
        for cl in unique(column)
            score = (column.==cl)'voting_coefficients
            if score > best
                best = score
                res = cl
            end
        end
        y_pred[i] = res
    end
    return y_pred
end

function vote(X, estimators)
    votes = hcat((h.predict(X) for h in estimators)...)
    y_pred = Vector(size(X,1))
    for i in 1:size(votes,1)
        column = votes[i,:][:]
        y_pred[i] = get_most_common_element(column)
    end
    return y_pred
end
