__precompile__()

"Ensemble models module"
module ensemble
export AdaBoostClassifier, RandomForestClassifier
include("../tree/decision_tree.jl")  # todo use modules
include("voting.jl")
include("adaboost.jl")
include("random_forest.jl")
end
