__precompile__()

"""Basic Ordinary Squares Linear regression
(supports only 1-dim values, included for learning purposes)"""
type BasicLinearRegression
    b0::Float64
    b1::Float64
    fit::Function
    predict::Function

    function BasicLinearRegression()
        this = new()
        this.fit = function (X_train, y_train)
            return fit!(this, X_train, y_train)
        end
        this.predict = function (X_test)
            return predict(this, X_test)
        end
        return this
    end

end

function fit!(regr::BasicLinearRegression, X_train, y_train)
    if ndims(X_train) != 1
        error("X_train should be 1-dimensional array for Simple Linear regression")
    end
    if ndims(y_train) != 1
        error("y_train should be 1-dimensional array for Simple Linear regression")
    end
    mean_x = mean(X_train)
    mean_y = mean(y_train)
    covariance = sum(( (X_train[i] - mean_x) * (y_train[i] - mean_y)  
                                         for i in 1:size(X_train,1) ))
    variance = sum(( (X_train[i] - mean_x)^2  
                                         for i in 1:size(X_train,1) ))
    regr.b1 = covariance/variance
    regr.b0 = mean_y - (regr.b1 * mean_x)
    return
end

function predict(regr::BasicLinearRegression, X_test)
    if ndims(X_test) != 1
        error("X_test should be 1-dimensional array for Simple Linear regression")
    end
    return [regr.b0 + regr.b1 * x for x in X_test]
end

