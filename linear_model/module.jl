__precompile__()

"Linear models module"
module linear_model
export LinearRegression, BasicLinearRegression
include("linear_regression.jl")
include("basic_linear_regression.jl")
end
