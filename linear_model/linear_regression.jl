__precompile__()

using IterativeSolvers: lsqr

"Linear regression fitted via solving least squares problem"
type LinearRegression
    "Same as coef_ in sklearn but transposed 
    (because of fortran-like arrays syntax in julia and lsqr)"
    coef::Any
    "Same as coef_ in sklearn"
    coef_::Function  # todo implement as property in next julia version
    "Same as intercept_ in sklearn"
    intercept_::Any

    fit_intercept::Bool
    """Normalization is not implemented yet. 
    Default value for this argument in sklearn is false."""
    normalize::Bool  # this field just for docstring and compatibility    

    fit::Function
    predict::Function

    function LinearRegression(;fit_intercept::Bool=true,
                              normalize::Bool=false)
        this = new()
        this.fit_intercept = fit_intercept
        this.normalize = normalize
        if normalize
            error("""Normalization is not implemented. 
                     Default value for this argument in sklearn is false
                     and you can preprocess data and normalize it independently.""")
        end

        this.fit = function (X_train, y_train)
            return fit!(this, X_train, y_train)
        end
        this.predict = function (X_test)
            return predict(this, X_test)
        end
        return this
    end
end

LinearRegression(fit_intercept::Bool, normalize::Bool) = 
LinearRegression(;fit_intercept=fit_intercept, normalize=normalize)
LinearRegression(fit_intercept::Bool) = LinearRegression(fit_intercept, false) 

# this function is not used now could be useful for normalization
function center_data(X, y, fit_intercept, normalize)
    if fit_intercept
        X_offset = mean(X, 1)
        X_cent = X .- X_offset
        y_offset = mean(y,1)
        y_cent = y .- y_offset
    else
        X_offset = zeros(size(X,2))
        # todo check it
        if ndim(y) == 1
            y_offset = 0
        else
            y_offset = zeros(size(y,2))
        end
    end
    return X_cent, y_cent, X_offset, y_offset
end

function _get_coef(X,y)
    coef = lsqr(X, y[:,1])
    for i in 2:size(y,2)
        res = lsqr(X, y[:,i])
        coef = hcat(coef,res)
    end
    return coef
end

function _convert_to_matrix(X::Array)
    if ndims(X) == 1
        return X[:,:]
    end
    return X
end

function _get_offset(X, y)
    X_offset = mean(X, 1)
    y_offset = mean(y, 1)
    return X .- X_offset, y.- y_offset, X_offset, y_offset
end

function fit!(regr::LinearRegression, X_train, y_train)
    X_train = _convert_to_matrix(X_train)
    if regr.fit_intercept
        X_train, y_train, X_offset, y_offset = _get_offset(X_train, y_train)
    else
        X_offset = y_offset = 0.
    end

    regr.coef = _get_coef(X_train, y_train)
    regr.intercept_ = y_offset - (X_offset * regr.coef)
    return
end

function predict(regr::LinearRegression, X_test)
    X_test = _convert_to_matrix(X_test)
    return X_test * regr.coef .+ regr.intercept_ 
end

